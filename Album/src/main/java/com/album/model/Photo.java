package com.album.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Photo {

	@Id
	private Integer idPhoto;
	
	@Column(name = "url", length = 50 )
	private String url;
	
	@Column(name = "descr", length = 50 )
	private String description;
	
	@Column(name = "crDate")
	@Temporal(TemporalType.DATE)
	private Date crDate; 

	@ManyToOne
	@JoinColumn(name="idAlbum")
    private Album Album;

	public Integer getIdPhoto() {
		return idPhoto;
	}

	public void setIdPhoto(Integer idPhoto) {
		this.idPhoto = idPhoto;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public Album getAlbum() {
		return Album;
	}

	public void setAlbum(Album album) {
		Album = album;
	}

}
