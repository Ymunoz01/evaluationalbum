package com.album.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.query.criteria.internal.expression.function.CurrentDateFunction;
import org.hibernate.query.criteria.internal.expression.function.CurrentTimeFunction;

@Entity
public class Album {

	@Id
	@Column(name = "idAlbum")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @PrimaryKeyJoinColumn
	private Integer idAlbum;
	
	@Column(name = "name", length = 50 )
	private String name;
	
	@Column(name = "crDate")
	@Temporal(TemporalType.DATE)
	private Date crDate;
	
	public Integer getIdAlbum() {
		return idAlbum;
	}

	public void setIdAlbum(Integer idAlbum) {
		this.idAlbum = idAlbum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@PrePersist
	protected void OnCreate() {
		crDate = new Date();
	}
	
	
}
