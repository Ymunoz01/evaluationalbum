package com.album.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.album.model.Album;

public interface IAlbumRepo extends JpaRepository<Album, Integer>{
	
}
