package com.album.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.album.model.Album;
import com.album.model.Photo;
import com.album.repository.IAlbumRepo;


@Service
public class AlbumServiceImpl implements IAlbumService {

	@Autowired
	private IAlbumRepo repo;

	@Override
	public Album create(Album album) {
		return repo.save(album);
	}

	@Override
	public void delete(Integer idAlbum) {
		repo.deleteById(idAlbum);
		
	}

	@Override
	public Optional<Album> findById(Integer idAlbum) {
		// TODO Auto-generated method stub
		return repo.findById(idAlbum);
	}

	/*@Override
	public Photo save(Photo photo) {
		// TODO Auto-generated method stub
		return repo.save(photo);
	}*/
	

}
