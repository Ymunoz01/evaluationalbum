package com.album.service;


import java.util.Optional;

import com.album.model.Album;

public interface IAlbumService {

	Album create(Album album);

    void delete(Integer idAlbum);

    Optional<Album> findById(Integer idAlbum);
	//Photo save(Photo photo);
}
