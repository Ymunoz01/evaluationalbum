package com.album.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.album.model.Album;
import com.album.service.AlbumServiceImpl;


@RestController
@RequestMapping("eymr/album")
public class AlbumController {

	@Autowired
	private AlbumServiceImpl albumServ;
	
	
	@PostMapping
    public ResponseEntity<Album> create(@Validated @RequestBody Album album){
        return new ResponseEntity<>(albumServ.create(album),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
    public ResponseEntity<Album> delete(@PathVariable("id") Integer idAlbum){
        return albumServ.findById(idAlbum)
                .map( c -> {
                    albumServ.delete(idAlbum);
                    return ResponseEntity.ok(c);
                })
                .orElseGet(()-> ResponseEntity.notFound().build());
    }
	
	
}
